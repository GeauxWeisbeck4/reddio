# We don't want to assign any expanded variables to $format (except $bp
# and $hr), so the surrounding double quotes are intended. $format later
# gets evaluated in cmd-print.sh
# shellcheck disable=2016

bp=$(printf '\xe2\x80\xa2') # Bullet point
hr='__________________________________________________'

# t1 Comments
format='${is_comment:+${no_color:+'$bp' }[$depth] ${no_color:+- }'\
'$fg7${up:+$bld$fg2}${down:+$bld$fg1}'\
'${show_score:+$score}${hide_score:+-}$rst$fg7'\
'${no_color:+${up:+(+)}${down:+(-)}} ${stickied:+[S] }'\
'${is_submitter:+$fg4${no_color:+[A] }}u/$author'\
'${distinguished:+[$distinguished]}${is_submitter:+$fg7} '\
'$created_pretty${edited:+*} $fg3$id$rst'\
'${is_mixed:+ ($link_id)$nl'\
'$fg7$link_title$rst (r/$subreddit)}$nl'\
'$text$nl$nl}'

# t2 User
format=$format'${is_user:+${is_mixed:+${no_color:+'$bp' }}'\
'u/$name${is_friend:+[F]} joined $created_pretty $fg3$id$rst$nl'\
'$comment_karma comment karma$nl'\
'$link_karma submission karma$nl'\
'${is_mixed:+$nl}}'

# t3 Submission (link)
format=$format'${is_t3:+${no_color:+${is_mixed:+'$bp' }'\
'${is_comments:+'$bp' }}$fg7${up:+$bld$fg2}${down:+$bld$fg1}'\
'${show_score:+$score}${hide_score:+-}${no_color:+${up:+(+)}'\
'${down:+(-)}}$rst$fg7 $title$rst ($domain)$nl'\
'$ul$url$rst$nl'\
'${tags:+$tags }$num_comments comment$comments_plural'\
' | $created_pretty${edited:+*} by ${is_comments:+$fg4}u/$author'\
'${distinguished:+[$distinguished]}$rst on r/$subreddit $fg3$id$rst$nl'\
'${is_comments:+${text:+$nl$text$nl}'\
'${is_gallery:+$nl'\
'$gallery$nl}'\
$hr'$nl}$nl}'

# t4 Messages
format=$format'${is_msg:+${no_color:+'$bp' }'\
'${fg7}${author:+u/}${author:=r/$subreddit}'\
'${distinguished:+[$distinguished]} to $dest $created_pretty'\
' $fg3$id${parent_id:+$rst ($parent_id)}$nl'\
'$fg7$subject$rst$nl'\
'$text$nl$nl}'

# t5 Subreddit
format=$format'${is_sub:+${is_mixed:+${no_color:+'$bp' }}'\
'r/$name $fg3$id$rst$nl'\
'$title${nl}'\
'A $type subreddit with $subscribers subscriber(s)'\
' created $created_pretty$nl${is_not_mixed:+'\
'${description:+'$hr'$nl$nl$description$nl}'\
'${submit_text:+'$hr'$nl$nl$submit_text$nl}'\
'${text:+'$hr'$nl$nl$text$nl}}'\
'${is_mixed:+$nl}}'

# More
format=$format'${is_more:+${no_color:+'$bp' }'\
'$fg7$count more$rst$nl$nl}'

# Continue
format=$format'${is_continue:+${fg7}Thread continues at$rst '\
'${fg3}comments/$link_id/comment/${parent_id#t1_}$rst$nl$nl}'

unset bp hr
